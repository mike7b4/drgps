# Drgps

Experimental [gpsd] (http://catb.org/gpsd/) "reinvent the wheel" written in Rust.

## Features:

* Asynchronous read using epoll via mio library.
* Only ublox (on Librem 5) has been tested.
* Only Linux supported for now.

## TODO

- [X] Unix socket support
- [X] TCP socket support similar to gpspipe
- [X] RAW -> Json
- [ ] Support OpenBSD and (http://redox-os.org "RedoxOS").
- [ ] Split daemon from lib code.

Well, actually it can not compete with gpsd... just yet ;)

## Dependies

* serde/serde_json
* toml
* nix
* termios
* mio
* nmea

## Install on librem

You can use debosirus to build the system on a X86 and then cargo deb to create a package to put on your librem5-devkit.
