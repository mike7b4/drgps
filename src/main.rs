use chrono::NaiveTime;
use core::time::Duration;
use mio::unix::EventedFd;
use mio::{Evented, Events, Poll, PollOpt, Ready, Token};
use nix::fcntl::{fcntl, OFlag, F_GETFL, F_SETFL};
use nmea::{FixType, Nmea};
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::fs::{File, OpenOptions};
use std::io;
use std::io::Read;
use std::io::Write;
use std::net::UdpSocket;
use std::net::{SocketAddr, TcpListener, TcpStream};
use std::os::unix::io::AsRawFd;
use std::os::unix::net::UnixListener;
use std::path::PathBuf;
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::mpsc::{channel, Receiver, SendError, Sender};
use std::sync::Arc;
use std::thread;
use std::time;
use structopt::StructOpt;
use termios::OPOST;
use termios::TCSANOW;
use termios::{cfsetspeed, tcflush, tcsetattr, Termios};
use termios::{CLOCAL, CREAD};
use termios::{ECHO, ECHOE, ECHOK, ECHONL, ICANON, IEXTEN, ISIG};
use termios::{ICRNL, IGNBRK, IGNCR, INLCR};
use termios::{VMIN, VTIME};
use toml;

// Copying from nmea GgaData
// Kind of an hack since I am lazy
// and don't want to implement deserialize manually for NMEA
#[derive(Serialize)]
struct GpsData {
    #[serde(skip)]
    _fix_time: Option<NaiveTime>,
    #[serde(skip)]
    _fix_type: Option<FixType>,
    latitude: Option<f64>,
    longitude: Option<f64>,
    fix_satellites: Option<u32>,
    hdop: Option<f32>,
    altitude: Option<f32>,
    geoid_height: Option<f32>,
}

struct Gps {
    handle: std::fs::File,
    termios: Termios,
    nmea: Nmea,
}

impl Gps {
    pub fn new(device: &PathBuf) -> Result<Self, std::io::Error> {
        let handle = OpenOptions::new().read(true).write(false).open(device)?;
        fcntl(
            handle.as_raw_fd(),
            F_SETFL(OFlag::O_NONBLOCK | OFlag::O_NOCTTY),
        )
        .expect("Async failed");
        fcntl(handle.as_raw_fd(), F_GETFL).expect("Async failed");
        let original_termios = Termios::from_fd(handle.as_raw_fd()).expect("termios failed");
        let mut termios = original_termios;
        // setup TTY for binary serial port access
        termios.c_cflag |= CREAD | CLOCAL;
        termios.c_lflag &= !(ICANON | ECHO | ECHOE | ECHOK | ECHONL | ISIG | IEXTEN);
        termios.c_oflag &= !OPOST;
        termios.c_iflag &= !(INLCR | IGNCR | ICRNL | IGNBRK);
        termios.c_iflag |= ICANON;
        termios.c_lflag |= ICANON;

        termios.c_cc[VMIN] = 0;
        termios.c_cc[VTIME] = 0;
        cfsetspeed(&mut termios, 9600).expect("Could not set baudrate");
        tcsetattr(handle.as_raw_fd(), TCSANOW, &termios).expect("Could not set attributes");
        if let Err(err) = tcflush(handle.as_raw_fd(), TCSANOW) {
            eprintln!("Flush failed with: {}", err);
        }
        Ok(Gps {
            handle,
            termios: original_termios,
            nmea: Nmea::new(),
        })
    }

    pub fn read_string(&mut self) -> Result<String, std::io::Error> {
        let mut data = String::new();
        match self.handle.read_to_string(&mut data) {
            Ok(_) | Err(_) => Ok(data),
        }
    }

    pub fn read_gna(&mut self) -> Option<GpsData> {
        if let Ok(data) = self.read_string() {
            if self.nmea.parse(&data.as_str()).is_ok() {
                return Some(GpsData {
                    _fix_time: self.nmea.fix_timestamp(),
                    _fix_type: self.nmea.fix_type(),
                    altitude: self.nmea.altitude,
                    fix_satellites: self.nmea.fix_satellites(),
                    geoid_height: self.nmea.geoid_height,
                    hdop: self.nmea.hdop(),
                    latitude: self.nmea.latitude(),
                    longitude: self.nmea.longitude(),
                });
            }
        }
        None
    }
}

impl Drop for Gps {
    fn drop(&mut self) {
        if let Err(err) = tcsetattr(self.handle.as_raw_fd(), TCSANOW, &self.termios) {
            eprintln!("Could not configure serial to default setting {}", err);
        }
    }
}

impl Evented for Gps {
    fn register(
        &self,
        poll: &Poll,
        token: Token,
        interest: Ready,
        opts: PollOpt,
    ) -> io::Result<()> {
        EventedFd(&self.handle.as_raw_fd()).register(poll, token, interest, opts)
    }

    fn reregister(
        &self,
        poll: &Poll,
        token: Token,
        interest: Ready,
        opts: PollOpt,
    ) -> io::Result<()> {
        EventedFd(&self.handle.as_raw_fd()).reregister(poll, token, interest, opts)
    }

    fn deregister(&self, poll: &Poll) -> io::Result<()> {
        EventedFd(&self.handle.as_raw_fd()).deregister(poll)
    }
}

struct Thread {
    handle: Option<thread::JoinHandle<()>>,
    run: Arc<AtomicBool>,
    tx: Sender<String>,
}

impl Thread {
    fn new(handle: thread::JoinHandle<()>, run: Arc<AtomicBool>, tx: Sender<String>) -> Self {
        Self {
            handle: Some(handle),
            run,
            tx,
        }
    }

    fn terminate(&mut self) {
        self.run.store(false, Ordering::Release);
        if let Some(thread) = self.handle.take() {
            thread.join().unwrap();
        }
    }

    fn send(&mut self, data: String) -> Result<(), SendError<String>> {
        self.tx.send(data)
    }
}

// Config file is stored in /etc/default/drgps.toml or can be overrided using --config-file
// If file not exist, or is wrongly configured, it will fallback to
// command line arguments passed to the drgps.
// The key is same as the variable names in Config struct
#[derive(StructOpt, Deserialize)]
struct Config {
    #[structopt(long = "config-file", default_value = "/etc/default/drgps.toml")]
    #[serde(skip)] // Command line only ignored in config file.
    config_file: PathBuf,
    #[structopt(short, long)]
    gpsdev: Option<PathBuf>,
    #[structopt(
        short = "t",
        long = "tcp-listen-address",
        help = "UDP listen IPAddress:port"
    )]
    tcp_listen_address: Option<SocketAddr>,
    #[structopt(
        short = "u",
        long = "udp-listen-address",
        help = "TCP listen IPAddress:port"
    )]
    udp_listen_address: Option<SocketAddr>,
    #[structopt(short = "i", long = "unix-path", help = "UNIX path to send GPS data")]
    unix_path: Option<PathBuf>,
    #[serde(default = "de_set_false")]
    #[structopt(short, long, help = "Enable verbose")]
    verbose: bool,
    #[serde(default = "de_set_false")]
    #[structopt(short, long, help = "Output to stdout")]
    stdout: bool,
    #[serde(default = "de_set_false")]
    #[structopt(short, long, help = "Output as json")]
    json: bool,
    #[serde(default = "de_default_ping_timeout")]
    #[structopt(
        short = "p",
        long = "udp-ping-timeout",
        help = "UDP client ping timeout (in seconds)",
        default_value = "10"
    )]
    udp_ping_timeout_seconds: u8,
}

// There must be a better way :(
fn de_set_false() -> bool {
    false
}
fn de_default_ping_timeout() -> u8 {
    10
}

fn ipc_listener(listen: PathBuf) -> Thread {
    let (tx, rx): (Sender<String>, Receiver<String>) = channel();
    let mut consumers = vec![];
    let listener = UnixListener::bind(&listen).expect("Could not setup IPC");
    listener
        .set_nonblocking(true)
        .expect("Could not set nonblocking");
    let terminated = Arc::new(AtomicBool::new(true));
    let term2 = terminated.clone();
    let handle = thread::spawn(move || {
        while terminated.load(Ordering::Relaxed) {
            if let Ok((stream, addr)) = listener.accept() {
                println!("caller {:?}", addr);
                consumers.push(stream);
            }

            if let Ok(data) = rx.try_recv() {
                let mut dead = vec![];
                for (id, stream) in consumers.iter_mut().enumerate() {
                    if let Err(e) = stream.write(data.as_bytes()) {
                        dead.push(id);
                        eprintln!("{}", e);
                    }
                }
                for id in dead {
                    consumers.remove(id);
                }
            }
            thread::sleep(Duration::from_millis(500));
        }
        drop(listener);
        if let Err(e) = std::fs::remove_file(listen) {
            eprintln!("{}", e);
        }
    });

    Thread::new(handle, term2, tx)
}

fn tcp_listener(listen: SocketAddr) -> Option<Thread> {
    let (tx, rx): (Sender<String>, Receiver<String>) = channel();
    let terminated = Arc::new(AtomicBool::new(true));
    let terminated2 = terminated.clone();
    let listener = match TcpListener::bind(listen) {
        Ok(listener) => listener,
        Err(err) => {
            eprintln!("TCP Bind failed: {}", err);
            return None;
        }
    };

    if let Err(e) = listener.set_nonblocking(true) {
        eprintln!("Could not set TCP listener to non blocking? {}", e);
        return None;
    }

    let handle = thread::spawn(move || {
        let mut consumers: Vec<TcpStream> = Vec::new();
        while terminated.load(Ordering::Relaxed) {
            for stream in listener.incoming() {
                match stream {
                    Ok(stream) => {
                        consumers.push(stream);
                    }
                    Err(ref e) if e.kind() == io::ErrorKind::WouldBlock => {
                        break;
                    }
                    Err(err) => {
                        eprintln!("{}", err);
                        terminated.store(true, Ordering::Relaxed);
                        break;
                    }
                };
            }
            if let Ok(data) = rx.try_recv() {
                let mut dead = vec![];
                for (id, stream) in consumers.iter_mut().enumerate() {
                    if let Err(e) = stream.write(data.as_bytes()) {
                        eprintln!("Connection dropped {}", e);
                        dead.push(id);
                    }
                }
                for id in dead {
                    consumers.remove(id);
                }
            }
            thread::sleep(Duration::from_millis(100));
        }
    });

    Some(Thread::new(handle, terminated2, tx))
}

fn udp_listener(listen: SocketAddr, ping_sec: u8) -> Thread {
    let (tx, rx): (Sender<String>, Receiver<String>) = channel();
    let terminated = Arc::new(AtomicBool::new(true));
    let terminate2 = terminated.clone();
    let listener = UdpSocket::bind(listen).expect("Could not setup IPC");
    listener
        .set_nonblocking(true)
        .expect("Could not set nonblocking");
    let handle = thread::spawn(move || {
        // The hashmap's value is a counter used for timeout check below.
        // We expect sender to send a "ping" at least every 10 seconds.
        // If no data received after ten second the connection is "closed"
        let mut consumers: HashMap<SocketAddr, u16> = HashMap::new();
        let mut handshake = [0; 8];
        while terminated.load(Ordering::Relaxed) {
            if let Ok((len, address)) = listener.recv_from(&mut handshake) {
                println!("got data len {} from: {:?}", len, address.ip());
                consumers.insert(address, ping_sec as u16 * 5);
            }
            if let Ok(data) = rx.try_recv() {
                for &address in consumers.clone().keys() {
                    if let Err(e) = listener.send_to(data.as_bytes(), address) {
                        eprintln!("{}", e);
                        consumers.remove(&address);
                    }
                }
            };
            thread::sleep(Duration::from_millis(200));
            for (&address, timeout) in &consumers.clone() {
                if *timeout == 0 {
                    consumers.remove(&address);
                } else {
                    *consumers.get_mut(&address).unwrap() -= 1;
                }
            }
        }
    });

    Thread::new(handle, terminate2, tx)
}

fn load_config() -> Config {
    let args = Config::from_args();
    let mut config = Config::from_args();
    if let Ok(mut file) = File::open(&config.config_file) {
        let mut data = String::new();
        file.read_to_string(&mut data)
            .expect("Given config file is invalid UTF8 and is not a TOML file.");
        config = match toml::from_str(&data) {
            Ok(config) => config,
            Err(err) => {
                eprintln!("Config file was invalid formated TOML\n{}\nfallback to command line arguments.", err);
                config
            }
        };

        if args.gpsdev.is_none() {
            config.gpsdev = args.gpsdev;
        }

        // command line options will always override config file.
        if args.tcp_listen_address != None {
            config.tcp_listen_address = args.tcp_listen_address;
        }

        if args.udp_listen_address != None {
            config.udp_listen_address = args.udp_listen_address;
        }

        if args.unix_path != None {
            config.unix_path = args.unix_path;
        }

        // Default is false for below fields but can be overided by command line.
        if args.verbose {
            config.verbose = true;
        }

        if args.stdout {
            config.stdout = true;
        }
        if args.json {
            config.json = true;
        }

        if args.udp_ping_timeout_seconds != 0 {
            config.udp_ping_timeout_seconds = args.udp_ping_timeout_seconds;
        }

        if config.udp_ping_timeout_seconds == 0 {
            config.udp_ping_timeout_seconds = 1;
        }
    } else {
        println!(
            "No '{:?}' file found, fallback and using command line arguments.",
            config.config_file
        );
    }

    config
}

fn main() -> std::io::Result<()> {
    let config = load_config();
    let mut threads: Vec<Thread> = Vec::new();
    let term = Arc::new(AtomicBool::new(false));
    signal_hook::flag::register(signal_hook::SIGQUIT, Arc::clone(&term)).unwrap();
    signal_hook::flag::register(signal_hook::SIGTERM, Arc::clone(&term)).unwrap();
    signal_hook::flag::register(signal_hook::SIGINT, Arc::clone(&term)).unwrap();

    let gpsdev = &config.gpsdev.unwrap_or_else(|| PathBuf::from("/dev/gps"));
    let mut gps = match Gps::new(gpsdev) {
        Ok(gps) => gps,
        Err(err) => {
            panic!(
                "Could not open device: '{}' {}.",
                gpsdev.to_str().unwrap(),
                err
            );
        }
    };

    if config.verbose {
        println!("GPS device: '{}'", gpsdev.to_str().unwrap_or("?error?"));
        if let Some(udp) = config.udp_listen_address {
            println!("UDP listen on: '{}'", udp);
            println!(
                "UDP Client ping timeout: {} seconds",
                config.udp_ping_timeout_seconds
            );
        }
        if let Some(tcp) = config.tcp_listen_address {
            println!("TCP listen on: '{}'", tcp);
        }
        if let Some(ipc) = config.unix_path.clone() {
            println!("UNIX listen on: '{}'", ipc.to_str().unwrap_or("?error?"));
        }

        thread::sleep(Duration::from_secs(2));
    }

    let poll = Poll::new()?;
    gps.register(&poll, Token(0), Ready::readable(), PollOpt::level())?;
    let mut events = Events::with_capacity(8);
    if let Some(ipc) = config.unix_path {
        threads.push(ipc_listener(ipc));
    }

    if let Some(addr) = config.tcp_listen_address {
        if let Some(tcp) = tcp_listener(addr) {
            threads.push(tcp);
        }
    }

    if let Some(udp) = config.udp_listen_address {
        threads.push(udp_listener(udp, config.udp_ping_timeout_seconds));
    }

    let mut terminate = false;
    let mut failcounter = 0;
    while !terminate {
        poll.poll(&mut events, Some(time::Duration::from_millis(250)))
            .expect("GPS poll error");
        for ev in &events {
            if ev.readiness() != Ready::readable() {
                panic!("readiness false");
            }

            let mut data = String::new();
            if config.json {
                if let Some(gn) = gps.read_gna() {
                    data = serde_json::to_string(&gn).unwrap_or_else(|_| String::from("{}"));
                }
            } else if let Ok(gn) = gps.read_string() {
                if gn.is_empty() {
                    failcounter += 1;
                    if failcounter == 5 {
                        eprintln!("GPS has not responded in a > 1 second? GPS dead?");
                        terminate = true;
                        break;
                    }
                } else {
                    failcounter = 0;
                    data = gn;
                }
            }

            if !data.is_empty() {
                let mut dead = vec![];
                if config.stdout {
                    print!("{}", data);
                }
                for (id, thread) in threads.iter_mut().enumerate() {
                    if thread.send(data.clone()).is_err() {
                        dead.push(id);
                    }
                }

                for id in dead {
                    println!("Dropped {}", id);
                    threads.remove(id);
                }
            }
        }

        if term.load(Ordering::Relaxed) {
            println!("Terminate");
            break;
        }
    }

    for thread in &mut threads {
        thread.terminate();
    }

    Ok(())
}
